package com.epam.training;

import com.epam.training.filter.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.*;

public class RoleManagerTest {

    private static final Set<Role> EXPECTED_ELEVATED_ROLES = new HashSet<>(Arrays.asList(Role.ADMIN, Role.DEVELOPER, Role.TRAINER, Role.USER));
    private static final Set<Role> EXPECTED_ROLES_ABOVE_ACCESS_LEVEL = new HashSet<>(Arrays.asList(Role.ADMIN, Role.USER, Role.TRAINER, Role.DEVELOPER));
    private static final Set<Role> EXPECTED_ROLES_BELOW_ACCESS_LEVEL = new HashSet<>(Collections.singletonList(Role.ASSISTANT));
    private static final Set<Role> EXPECTED_ROLES_ON_ACCESS_LEVEL = new HashSet<>(Arrays.asList(Role.DEVELOPER));
    private static final int ACCESS_LEVEL = 5;

    private RoleManager underTest;

    @BeforeClass
    public void setUp() {
        underTest = new RoleManager();
    }


    /**
     * Test cases for the old solution.
     */
    @Test
    public void testGetElevatedRoles() {
        //GIVEN
        //WHEN
        Set<Role> actual = underTest.getElevatedRoles();
        //THEN
        assertEquals(actual, EXPECTED_ELEVATED_ROLES);
    }

    @Test
    public void testGetRolesAboveAccessLevel() {
        //GIVEN
        //WHEN
        Set<Role> actual = underTest.getRolesAboveAccessLevel(ACCESS_LEVEL);
        //THEN
        assertEquals(actual, EXPECTED_ROLES_ABOVE_ACCESS_LEVEL);
    }

    @Test
    public void testGetRolesBelowAccessLevel() {
        //GIVEN
        //WHEN
        Set<Role> actual = underTest.getRolesBelowAccessLevel(ACCESS_LEVEL);
        //THEN
        assertEquals(actual, EXPECTED_ROLES_BELOW_ACCESS_LEVEL);
    }

    @Test
    public void testGetElevatedRolesOnAccessLevel() {
        //GIVEN
        //WHEN
        Set<Role> actual = underTest.getElevatedRolesOnAccessLevel(ACCESS_LEVEL);
        //THEN
        assertEquals(actual, EXPECTED_ROLES_ON_ACCESS_LEVEL);
    }

    /**
     * Test cases for the new solution with dataProvider.
     */
    @Test(dataProvider = "provideData")
    public void testGetRoles(final RolePredicate rolePredicate, final Set<Role> expected) {
        //GIVEN
        //WHEN
        Set<Role> actual = underTest.getRoles(rolePredicate);
        //THEN
        assertEquals(actual, expected);
    }

    @DataProvider
    public Object[][] provideData() {
        return new Object[][]{
                {new EnabledRolePredicate(), EXPECTED_ELEVATED_ROLES},
                {new AboveAccessLevelRolePredicate(ACCESS_LEVEL), EXPECTED_ROLES_ABOVE_ACCESS_LEVEL},
                {new BelowAccessLevelRolePredicate(ACCESS_LEVEL), EXPECTED_ROLES_BELOW_ACCESS_LEVEL},
                {new OnAccessLevelRolePredicate(ACCESS_LEVEL), EXPECTED_ROLES_ON_ACCESS_LEVEL},
        };
    }
}
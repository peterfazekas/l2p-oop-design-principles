package com.epam.training.filter;

import com.epam.training.Role;

public class OnAccessLevelRolePredicate implements RolePredicate {

    private final int accessLevel;

    public OnAccessLevelRolePredicate(final int accessLevel) {
        this.accessLevel = accessLevel;
    }

    @Override
    public boolean test(final Role role) {
        return role.isEnabled() && role.isElevated() && role.getAccessLevel() == accessLevel;
    }
}

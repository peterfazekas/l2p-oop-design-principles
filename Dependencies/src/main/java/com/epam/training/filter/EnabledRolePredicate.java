package com.epam.training.filter;

import com.epam.training.Role;

public class EnabledRolePredicate implements RolePredicate {

    @Override
    public boolean test(final Role role) {
        return role.isEnabled() && role.isElevated();
    }
}

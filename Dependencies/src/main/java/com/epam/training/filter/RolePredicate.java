package com.epam.training.filter;

import com.epam.training.Role;

public interface RolePredicate {

    boolean test(Role role);
}

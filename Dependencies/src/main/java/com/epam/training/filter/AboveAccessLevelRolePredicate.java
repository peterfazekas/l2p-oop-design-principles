package com.epam.training.filter;

import com.epam.training.Role;

public class AboveAccessLevelRolePredicate implements RolePredicate {

    private final int accessLevel;

    public AboveAccessLevelRolePredicate(final int accessLevel) {
        this.accessLevel = accessLevel;
    }

    @Override
    public boolean test(final Role role) {
        return role.isEnabled() && role.getAccessLevel() >= accessLevel;
    }
}

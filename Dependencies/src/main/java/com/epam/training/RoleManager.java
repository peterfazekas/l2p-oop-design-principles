package com.epam.training;

import com.epam.training.filter.RolePredicate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RoleManager {

	/**
	 * This is the refactored RoleManager class content
 	 * @param rolePredicate abstract predicate
	 * @return set of certain roles.
	 */
	public Set<Role> getRoles(final RolePredicate rolePredicate) {
		return Arrays.stream(Role.values())
				.filter(rolePredicate::test)
				.collect(Collectors.toSet());
	}

	/**
	 * From here you can find the old RoleManager class content.
	 * I left as it was because this way I could assured with UnitTest that all solution leaded to the same result.
	 */
	public Set<Role> getElevatedRoles() {
		Set<Role> roles = new HashSet<Role>();
		for (Role currentRole : Role.values()) {
			if (currentRole.isEnabled() && currentRole.isElevated()) {
				roles.add(currentRole);
			}
		}
		return roles;
	}
	
	public Set<Role> getRolesAboveAccessLevel(int accessLevel) {
		Set<Role> roles = new HashSet<Role>();
		for (Role currentRole : Role.values()) {
			if (currentRole.getAccessLevel() >= accessLevel) {
				if (currentRole.isEnabled()) {
					roles.add(currentRole);
				}
			}
		}
		return roles;
	}
	
	public Set<Role> getRolesBelowAccessLevel(int accessLevel) {
		Set<Role> roles = new HashSet<Role>();
		for (Role currentRole : Role.values()) {
			if (currentRole.getAccessLevel() < accessLevel && currentRole.isEnabled()) {
				roles.add(currentRole);
			}
		}
		return roles;
	}
	
	public Set<Role> getElevatedRolesOnAccessLevel(int accessLevel) {
		Set<Role> roles = new HashSet<Role>();
		for (Role currentRole : Role.values()) {
			if (currentRole.getAccessLevel() == accessLevel && currentRole.isEnabled() && currentRole.isElevated()) {
				roles.add(currentRole);
			}
		}
		return roles;
	}

}

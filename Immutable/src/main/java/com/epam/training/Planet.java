package com.epam.training;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class Planet {

	private final String name;
	private final Date dateOfDiscovery;
	private final List<Moon> moons;

	public Planet(final String name, final Date dateOfDiscovery, final List<Moon> moons) {
		this.name = name;
		this.dateOfDiscovery = new Date(dateOfDiscovery.getTime());
		this.moons = new ArrayList<>(moons);
	}
o
	public String getName() {
		return name;
	}

	public Date getDateOfDiscovery() {
		return new Date(dateOfDiscovery.getTime());
	}

	public List<Moon> getMoons() {
		return Collections.unmodifiableList(moons);
	}

}

package com.epam.training;


public final class Moon {

	private final Integer size;

	public Moon(final Integer size) {
		this.size = size;
	}

	public Integer getSize() {
		return size;
	}
}

package com.epam;


import com.epam.domain.Creature;

import java.util.Arrays;
import java.util.List;

public class Application {

	private static List<Creature> creatures = Arrays.asList(Creature.DOG, Creature.DOG, Creature.DOG, Creature.CAT, Creature.DUCK);

	public static void main(String[] args) {
		Farm farm = new Farm(creatures);
		farm.sing();
	}
	
}

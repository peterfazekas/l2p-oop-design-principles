package com.epam;

import com.epam.domain.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Farm {

    private final List<Animal> animals;

    public Farm(final List<Creature> creatures) {
        this.animals = create(creatures);
    }

    public void sing() {
        animals.forEach(this::print);
    }

    private List<Animal> create(final List<Creature> creatures) {
        return creatures.stream()
                .map(Creature::create)
                .collect(Collectors.toList());
    }

    private void print(final Animal animal) {
        System.out.println(animal.speak());
    }
}

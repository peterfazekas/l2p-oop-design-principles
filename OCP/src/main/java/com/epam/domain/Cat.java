package com.epam.domain;

class Cat implements Animal {

    private static final String SAY = "Meow!";

    @Override
    public String speak() {
        return SAY;
    }
}

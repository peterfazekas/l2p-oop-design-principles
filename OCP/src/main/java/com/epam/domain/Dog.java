package com.epam.domain;

class Dog implements Animal {

    private static final String SAY = "Woof!";

    @Override
    public String speak() {
        return SAY;
    }
}

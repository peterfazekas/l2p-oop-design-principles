package com.epam.domain;

public interface Animal {

	String speak();
}

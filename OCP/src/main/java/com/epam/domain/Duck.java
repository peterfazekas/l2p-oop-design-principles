package com.epam.domain;

class Duck implements Animal {

    private static final String SAY = "Quack!";

    @Override
    public String speak() {
        return SAY;
    }
}

package com.epam.domain;

public enum Creature {

    DOG(new Dog()),
    CAT(new Cat()),
    DUCK(new Duck());

    private final Animal animal;

    Creature(final Animal animal) {
        this.animal = animal;
    }

    public Animal create() {
        return animal;
    }
}

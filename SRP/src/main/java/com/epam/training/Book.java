package com.epam.training;

public class Book {

	private Author author;
	private int yearOfPublication;
	private String title;
	private String description;

	public void setAuthor(final String authorName, final String authorDescription) {
		this.author = new Author(authorName, authorDescription);
	}

	public int getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(int yearOfPublication) {
		this.yearOfPublication = yearOfPublication;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthorName() {
		return author.getAuthorName();
	}

	public String getAuthorDescription() {
		return author.getAuthorDescription();
	}

	@Override
	public String toString() {
		return "Book [" + author + ", yearOfPublication=" + yearOfPublication + ", title='" + title + "', description='"
				+ description + "']";
	}

}

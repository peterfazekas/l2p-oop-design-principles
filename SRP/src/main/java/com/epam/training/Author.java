package com.epam.training;

public class Author {

    private final String authorName;
    private final String authorDescription;

    public Author(final String authorName, final String authorDescription) {
        this.authorName = authorName;
        this.authorDescription = authorDescription;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorDescription() {
        return authorDescription;
    }

    @Override
    public String toString() {
        return "authorName='" + authorName + "', authorDescription='" + authorDescription + '\'';
    }
}

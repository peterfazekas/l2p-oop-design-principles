package com.epam.store;

import java.math.BigDecimal;

import com.epam.Product;
import com.epam.data.DataStore;
import com.epam.price.PriceCalculator;

public class Store {

	private final DataStore<Product> dataStore;
	private final PriceCalculator priceCalculator;

	public Store(final DataStore<Product> dataStore, final PriceCalculator priceCalculator) {
		this.dataStore = dataStore;
		this.priceCalculator = priceCalculator;
	}


	public void open() {
		dataStore.add(new Product("Book", new BigDecimal("100")));
		dataStore.add(new Product("UberLaptop", new BigDecimal("10000")));
	}

	public void printPrices() {
		dataStore.list().forEach(product -> System.out.println(printItem(product)));
	}

	private String printItem(Product product) {
		return product.getName() + ": " + priceCalculator.price(product);
	}

	public BigDecimal stock() {
		BigDecimal sum = BigDecimal.ZERO;
		for (Product product : dataStore.list()) {
			sum = sum.add(priceCalculator.price(product));
		}
		return sum;
	}

}

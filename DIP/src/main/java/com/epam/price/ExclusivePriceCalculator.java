package com.epam.price;

import java.math.BigDecimal;

import com.epam.Product;

public class ExclusivePriceCalculator implements PriceCalculator {

	private static final String VALUE = "2";
	private static final BigDecimal MULTIPLIER = new BigDecimal(VALUE);

	@Override
	public BigDecimal price(final Product product) {
		return product.getPrice().multiply(MULTIPLIER);
	}

}

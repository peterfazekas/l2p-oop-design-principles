package com.epam.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MemoryDataStore<T> implements DataStore<T> {

	private List<T> store = new ArrayList<>();

	@Override
	public void add(final T t) {
		store.add(t);
	}

	@Override
	public Iterable<T> list() {
		return Collections.unmodifiableCollection(store);
	}

}

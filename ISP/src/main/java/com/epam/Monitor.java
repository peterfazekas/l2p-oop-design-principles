package com.epam;

import com.epam.cache.CacheMonitor;

public class Monitor {

	private CacheMonitor cache;

	public Monitor(CacheMonitor cacheMonitor) {
		this.cache = cacheMonitor;
	}
	
	public void printInfo() {
		System.out.println("Cache info:");
		for (String key : cache.getKeys()) {
			System.out.println("Element: " + key);
			System.out.println("Last access: " + cache.getLastAccess(key));
			System.out.println("Number of hits: " + cache.getNumHits(key));
		}
	}
	
}

package com.epam;

import com.epam.cache.BestCache;
import com.epam.cache.Cache;
import com.epam.cache.CacheMonitor;

public class Application {

	public static void main(String[] args) {
		BestCache<String> stringCache = new BestCache<>();
		use(stringCache);
		monitor(stringCache);
	}

	private static void monitor(CacheMonitor stringCache) {
		Monitor stringCacheMonitor = new Monitor(stringCache);
		stringCacheMonitor.printInfo();
	}

	private static void use(Cache<String> stringCache) {
		CacheUser cacheUser = new CacheUser(stringCache);
		cacheUser.use();
	}

}

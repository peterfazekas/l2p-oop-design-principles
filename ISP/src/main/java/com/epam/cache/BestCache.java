package com.epam.cache;

import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;

public class BestCache<T> implements Cache<T>, CacheMonitor{

	private Map<String, Entry<T>> cache = new WeakHashMap<>();
	
	@Override
	public void put(final String key, T value) {
		cache.put(key, new Entry<>(value));
	}

	@Override
	public T get(final String key) {
		return cache.get(key).getValue();
	}

	@Override
	public void clear(final String key) {
		cache.remove(key);
	}

	@Override
	public void clearAll() {
		cache.clear();
	}
	
	@Override
	public Iterable<String> getKeys() {
		return cache.keySet();
	}

	@Override
	public Date getLastAccess(final String key) {
		return cache.get(key).getLastAccess();
	}

	@Override
	public long getNumHits(final String key) {
		return cache.get(key).getHits();
	}

}

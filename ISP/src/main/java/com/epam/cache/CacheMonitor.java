package com.epam.cache;

import java.util.Date;

public interface CacheMonitor {

	Iterable<String> getKeys();
	
	Date getLastAccess(String key);

	long getNumHits(String key);

}

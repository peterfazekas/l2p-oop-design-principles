package com.epam.cache;

import java.util.Date;

class Entry<T> {

    private T value;
    private Date lastAccess;
    private long hits;

    Entry(final T value) {
        this.value = value;
        this.lastAccess = new Date();
        this.hits = 1L;
    }

    public T getValue() {
        incHits();
        updateLastAccess();
        return value;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public long getHits() {
        return hits;
    }

    private void incHits() {
        hits++;
    }

    private void updateLastAccess() {
        this.lastAccess = new Date();
    }
}

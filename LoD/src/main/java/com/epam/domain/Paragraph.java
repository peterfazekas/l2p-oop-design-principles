package com.epam.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class Paragraph {

	private final List<Line> lines;

	public Paragraph(final Collection<Line> lines) {
		this.lines = new ArrayList<>(lines);
	}

	public void print() {
		lines.forEach(Line::print);
	}

}

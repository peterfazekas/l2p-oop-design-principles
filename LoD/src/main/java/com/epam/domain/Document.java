package com.epam.domain;

import java.util.ArrayList;
import java.util.List;

public final class Document {

	private final List<Paragraph> paragraphs;

	public Document(final List<Paragraph> paragraphs) {
		this.paragraphs = new ArrayList<>(paragraphs);
	}

	public void print() {
		paragraphs.forEach(Paragraph::print);
	}
}

package com.epam;

import com.epam.domain.Document;

public class Application {

	private void start() {
		Document document = new DocumentCreator().createDocument();
		printDocument(document);
	}

	private void printDocument(final Document document) {
		document.print();
	}

	public static void main(String[] args) {
		new Application().start();
	}

}

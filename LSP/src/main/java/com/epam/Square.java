package com.epam;

public class Square implements Quadrilateral{

	protected double sideA;

	public Square(double sideA) {
		this.sideA = sideA;
	}

	public void setSize(final double sideA) {
		this.sideA = sideA;
	}

	public double getSide() {
		return sideA;
	}

	@Override
	public double area() {
		return sideA * sideA;
	}

	@Override
	public double perimeter() {
		return 4 * sideA ;
	}

	@Override
	public String getProperty() {
		return String.format("- Type: Square,\t\tArea: %2.2f, Perimeter: %2.2f", area(), perimeter());
	}
}

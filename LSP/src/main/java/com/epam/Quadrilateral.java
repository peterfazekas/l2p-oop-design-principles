package com.epam;

public interface Quadrilateral {

    double area();

    double perimeter();

    String getProperty();

}

package com.epam;

public class Rectangle extends Square{

	private double sideB;

	public Rectangle(final double sideA, final double sideB) {
		super(sideA);
		this.sideB = sideB;
	}

	public double getSideA() {
		return super.getSide();
	}

	public double getSideB() {
		return sideB;
	}

	@Override
	public double area() {
		return sideA * sideB;
	}

	@Override
	public double perimeter() {
		return 2 * (sideA + sideB);
	}

	@Override
	public String getProperty() {
		return String.format("- Type: Rectangle,\tArea: %2.2f, Perimeter: %2.2f", area(), perimeter());
	}

}

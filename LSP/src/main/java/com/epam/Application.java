package com.epam;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Application {

    private static final String DELIMITER = "\n\r";
    private List<Quadrilateral> rectangles = Arrays.asList(new Rectangle(10, 20), new Square(10), new Rectangle(30, 40));

    public static void main(String[] args) {
        Application application = new Application();
        application.printProperties();
    }

    public void printProperties() {
        System.out.println("Properties:");
        System.out.println(getProperties());
    }

    private String getProperties() {
        return rectangles.stream()
                .map(Quadrilateral::getProperty)
                .collect(Collectors.joining(DELIMITER));
    }


}
